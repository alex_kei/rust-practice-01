 Simple usage

## Build progect

Run in console:

```
cargo build
```

Then yo can check for executable in `./target/debug`.
It is `./target/debug/guessing_game`.


## Run program

To run program you can use built executable (see previous section)
or you can run in console:

```
cargo run
```

## Content

+ `fibonacci` - function to calculate n-th Fibonacci number;
+ Set of functions for temperature convertion:
    * `fahrenheit_to_celsius` - converts temperature in Fahrenheit degrees to Celsius;
    * `fahrenheit_to_kelvin` - converts temperature in Fahrenheit degrees to Kelvin;
    * `celsius_to_fahrenheit` - converts temperature in Celsius degrees to Fahrenheit;
    * `celsius_to_kelvin` - converts temperature in Celsius degrees to Kelvin;
    * `kelvin_to_fahrenheit` - converts temperature in Kelvin degrees to Fahrenheit;
    * `kelvin_to_celsius` - converts temperature in Kelvin degrees to Celsius;
