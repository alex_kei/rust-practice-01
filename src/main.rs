fn main() {
    for n in 1..20 {
        println!("Fibonacci number {} is {}", n, fibonacci(n));
    }

    println!();
    println!("Temperature conversion");

    println!();
    println!("Fahrenheit - Celsius:");
    println!("{}F is {}C", 1.0, fahrenheit_to_celsius(1.0));
    println!("{}C is {}F", -17.222221, celsius_to_fahrenheit(-17.222221));
    println!("{}C is {}F", 1.0, celsius_to_fahrenheit(1.0));
    println!("{}F is {}C", 33.8, fahrenheit_to_celsius(33.8));

    println!();
    println!("Celsius - Kelvin:");
    println!("{}C is {}K", 1.0, celsius_to_kelvin(1.0));
    println!("{}K is {}C", 274.15, kelvin_to_celsius(274.15));
    println!("{}K is {}C", 1.0, kelvin_to_celsius(1.0));
    println!("{}C is {}K", -272.15, celsius_to_kelvin(-272.15));

    println!();
    println!("Kelvin - Fahrenheit:");
    println!("{}K is {}F", 1.0, kelvin_to_fahrenheit(1.0));
    println!("{}F is {}K", -457.87003, fahrenheit_to_kelvin(-457.87003));
    println!("{}F is {}K", 1.0, fahrenheit_to_kelvin(1.0));
    println!("{}K is {}F", 255.9278, kelvin_to_fahrenheit(255.9278));
}

fn fibonacci(n: u32) -> u64 {
    if n < 3 {
        return 1;
    }

    let mut index = 3;
    let mut current = 2;
    let mut previous = 1;

    while index < n {
        let tmp = current;
        current += previous;
        previous = tmp;
        index += 1;
    }
    return current;
}

fn fahrenheit_to_celsius(temperature_in_fahrenheir: f32) -> f32 {
    (temperature_in_fahrenheir - 32.0) * 5.0 / 9.0
}

fn fahrenheit_to_kelvin(temperature_in_fahrenheir: f32) -> f32 {
    (temperature_in_fahrenheir + 459.67) * 5.0 / 9.0
}

fn celsius_to_fahrenheit(temperature_in_celsius: f32) -> f32 {
    temperature_in_celsius * 9.0 / 5.0 + 32.0
}

fn celsius_to_kelvin(temperature_in_celsius: f32) -> f32 {
    temperature_in_celsius + 273.15
}

fn kelvin_to_fahrenheit(temperature_in_kelvin: f32) -> f32 {
    temperature_in_kelvin * 9.0 / 5.0 - 459.67
}

fn kelvin_to_celsius(temperature_in_kelvin: f32) -> f32 {
    temperature_in_kelvin - 273.15
}

